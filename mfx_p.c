

/* this ALWAYS GENERATED file contains the proxy stub code */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Thu May 09 21:40:24 2013
 */
/* Compiler settings for mfx.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#if !defined(_M_IA64) && !defined(_M_AMD64)


#pragma warning( disable: 4049 )  /* more than 64k source lines */
#if _MSC_VER >= 1200
#pragma warning(push)
#endif

#pragma warning( disable: 4211 )  /* redefine extern to static */
#pragma warning( disable: 4232 )  /* dllimport identity*/
#pragma warning( disable: 4024 )  /* array to pointer mapping*/
#pragma warning( disable: 4152 )  /* function/data pointer conversion in expression */
#pragma warning( disable: 4100 ) /* unreferenced arguments in x86 call */

#pragma optimize("", off ) 

#define USE_STUBLESS_PROXY


/* verify that the <rpcproxy.h> version is high enough to compile this file*/
#ifndef __REDQ_RPCPROXY_H_VERSION__
#define __REQUIRED_RPCPROXY_H_VERSION__ 475
#endif


#include "rpcproxy.h"
#ifndef __RPCPROXY_H_VERSION__
#error this stub requires an updated version of <rpcproxy.h>
#endif /* __RPCPROXY_H_VERSION__ */


#include "mfx_h.h"

#define TYPE_FORMAT_STRING_SIZE   3                                 
#define PROC_FORMAT_STRING_SIZE   1                                 
#define EXPR_FORMAT_STRING_SIZE   1                                 
#define TRANSMIT_AS_TABLE_SIZE    0            
#define WIRE_MARSHAL_TABLE_SIZE   0            

typedef struct _mfx_MIDL_TYPE_FORMAT_STRING
    {
    short          Pad;
    unsigned char  Format[ TYPE_FORMAT_STRING_SIZE ];
    } mfx_MIDL_TYPE_FORMAT_STRING;

typedef struct _mfx_MIDL_PROC_FORMAT_STRING
    {
    short          Pad;
    unsigned char  Format[ PROC_FORMAT_STRING_SIZE ];
    } mfx_MIDL_PROC_FORMAT_STRING;

typedef struct _mfx_MIDL_EXPR_FORMAT_STRING
    {
    long          Pad;
    unsigned char  Format[ EXPR_FORMAT_STRING_SIZE ];
    } mfx_MIDL_EXPR_FORMAT_STRING;


static const RPC_SYNTAX_IDENTIFIER  _RpcTransferSyntax = 
{{0x8A885D04,0x1CEB,0x11C9,{0x9F,0xE8,0x08,0x00,0x2B,0x10,0x48,0x60}},{2,0}};


extern const mfx_MIDL_TYPE_FORMAT_STRING mfx__MIDL_TypeFormatString;
extern const mfx_MIDL_PROC_FORMAT_STRING mfx__MIDL_ProcFormatString;
extern const mfx_MIDL_EXPR_FORMAT_STRING mfx__MIDL_ExprFormatString;


extern const MIDL_STUB_DESC Object_StubDesc;


extern const MIDL_SERVER_INFO IMfxKeySigMap_ServerInfo;
extern const MIDL_STUBLESS_PROXY_INFO IMfxKeySigMap_ProxyInfo;


extern const MIDL_STUB_DESC Object_StubDesc;


extern const MIDL_SERVER_INFO IMfxInstruments_ServerInfo;
extern const MIDL_STUBLESS_PROXY_INFO IMfxInstruments_ProxyInfo;


extern const MIDL_STUB_DESC Object_StubDesc;


extern const MIDL_SERVER_INFO IMfxNameListSet_ServerInfo;
extern const MIDL_STUBLESS_PROXY_INFO IMfxNameListSet_ProxyInfo;


extern const MIDL_STUB_DESC Object_StubDesc;


extern const MIDL_SERVER_INFO IMfxNameList_ServerInfo;
extern const MIDL_STUBLESS_PROXY_INFO IMfxNameList_ProxyInfo;


extern const MIDL_STUB_DESC Object_StubDesc;


extern const MIDL_SERVER_INFO IMfxSelection_ServerInfo;
extern const MIDL_STUBLESS_PROXY_INFO IMfxSelection_ProxyInfo;


extern const MIDL_STUB_DESC Object_StubDesc;


extern const MIDL_SERVER_INFO IMfxInputPulse_ServerInfo;
extern const MIDL_STUBLESS_PROXY_INFO IMfxInputPulse_ProxyInfo;


extern const MIDL_STUB_DESC Object_StubDesc;


extern const MIDL_SERVER_INFO IMfxDocumentFactory_ServerInfo;
extern const MIDL_STUBLESS_PROXY_INFO IMfxDocumentFactory_ProxyInfo;


extern const MIDL_STUB_DESC Object_StubDesc;


extern const MIDL_SERVER_INFO IMfxDocument_ServerInfo;
extern const MIDL_STUBLESS_PROXY_INFO IMfxDocument_ProxyInfo;


extern const MIDL_STUB_DESC Object_StubDesc;


extern const MIDL_SERVER_INFO IMfxMarkerMap_ServerInfo;
extern const MIDL_STUBLESS_PROXY_INFO IMfxMarkerMap_ProxyInfo;


extern const MIDL_STUB_DESC Object_StubDesc;


extern const MIDL_SERVER_INFO IMfxBufferFactory_ServerInfo;
extern const MIDL_STUBLESS_PROXY_INFO IMfxBufferFactory_ProxyInfo;



#if !defined(__RPC_WIN32__)
#error  Invalid build platform for this stub.
#endif

#if !(TARGET_IS_NT50_OR_LATER)
#error You need Windows 2000 or later to run this stub because it uses these features:
#error   /robust command line switch.
#error However, your C/C++ compilation flags indicate you intend to run this app on earlier systems.
#error This app will fail with the RPC_X_WRONG_STUB_VERSION error.
#endif


static const mfx_MIDL_PROC_FORMAT_STRING mfx__MIDL_ProcFormatString =
    {
        0,
        {

			0x0
        }
    };

static const mfx_MIDL_TYPE_FORMAT_STRING mfx__MIDL_TypeFormatString =
    {
        0,
        {
			NdrFcShort( 0x0 ),	/* 0 */

			0x0
        }
    };


/* Standard interface: __MIDL_itf_mfx_0000_0000, ver. 0.0,
   GUID={0x00000000,0x0000,0x0000,{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}} */


/* Object interface: IUnknown, ver. 0.0,
   GUID={0x00000000,0x0000,0x0000,{0xC0,0x00,0x00,0x00,0x00,0x00,0x00,0x46}} */


/* Object interface: IMfxEventFilter, ver. 0.0,
   GUID={0x8D7FDE01,0x1B1B,0x11D2,{0xA8,0xE0,0x00,0x00,0xA0,0x09,0x0D,0xAF}} */


/* Standard interface: __MIDL_itf_mfx_0000_0001, ver. 0.0,
   GUID={0x00000000,0x0000,0x0000,{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}} */


/* Object interface: IMfxEventQueue, ver. 0.0,
   GUID={0xFF76F7C2,0xF166,0x11D1,{0xA8,0xE0,0x00,0x00,0xA0,0x09,0x0D,0xAF}} */


/* Object interface: IMfxDataQueue, ver. 0.0,
   GUID={0x7A37A621,0x1B1B,0x11D2,{0xA8,0xE0,0x00,0x00,0xA0,0x09,0x0D,0xAF}} */


/* Object interface: IMfxTempoMap, ver. 0.0,
   GUID={0xFF76F7C3,0xF166,0x11D1,{0xA8,0xE0,0x00,0x00,0xA0,0x09,0x0D,0xAF}} */


/* Standard interface: __MIDL_itf_mfx_0000_0004, ver. 0.0,
   GUID={0x00000000,0x0000,0x0000,{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}} */


/* Object interface: IMfxMeterMap, ver. 0.0,
   GUID={0xFF76F7C4,0xF166,0x11D1,{0xA8,0xE0,0x00,0x00,0xA0,0x09,0x0D,0xAF}} */


/* Object interface: IMfxKeySigMap, ver. 0.0,
   GUID={0xA489E601,0x0247,0x11D2,{0xA8,0xE0,0x00,0x00,0xA0,0x09,0x0B,0x97}} */

#pragma code_seg(".orpc")
static const unsigned short IMfxKeySigMap_FormatStringOffsetTable[] =
    {
    0
    };

static const MIDL_STUBLESS_PROXY_INFO IMfxKeySigMap_ProxyInfo =
    {
    &Object_StubDesc,
    mfx__MIDL_ProcFormatString.Format,
    &IMfxKeySigMap_FormatStringOffsetTable[-3],
    0,
    0,
    0
    };


static const MIDL_SERVER_INFO IMfxKeySigMap_ServerInfo = 
    {
    &Object_StubDesc,
    0,
    mfx__MIDL_ProcFormatString.Format,
    &IMfxKeySigMap_FormatStringOffsetTable[-3],
    0,
    0,
    0,
    0};
CINTERFACE_PROXY_VTABLE(3) _IMfxKeySigMapProxyVtbl = 
{
    0,
    &IID_IMfxKeySigMap,
    IUnknown_QueryInterface_Proxy,
    IUnknown_AddRef_Proxy,
    IUnknown_Release_Proxy
};

const CInterfaceStubVtbl _IMfxKeySigMapStubVtbl =
{
    &IID_IMfxKeySigMap,
    &IMfxKeySigMap_ServerInfo,
    3,
    0, /* pure interpreted */
    CStdStubBuffer_METHODS
};


/* Object interface: IMfxInstruments, ver. 0.0,
   GUID={0x6BFCA001,0x043D,0x11D2,{0xA8,0xE0,0x00,0x00,0xA0,0x09,0x0D,0xAF}} */

#pragma code_seg(".orpc")
static const unsigned short IMfxInstruments_FormatStringOffsetTable[] =
    {
    0
    };

static const MIDL_STUBLESS_PROXY_INFO IMfxInstruments_ProxyInfo =
    {
    &Object_StubDesc,
    mfx__MIDL_ProcFormatString.Format,
    &IMfxInstruments_FormatStringOffsetTable[-3],
    0,
    0,
    0
    };


static const MIDL_SERVER_INFO IMfxInstruments_ServerInfo = 
    {
    &Object_StubDesc,
    0,
    mfx__MIDL_ProcFormatString.Format,
    &IMfxInstruments_FormatStringOffsetTable[-3],
    0,
    0,
    0,
    0};
CINTERFACE_PROXY_VTABLE(3) _IMfxInstrumentsProxyVtbl = 
{
    0,
    &IID_IMfxInstruments,
    IUnknown_QueryInterface_Proxy,
    IUnknown_AddRef_Proxy,
    IUnknown_Release_Proxy
};

const CInterfaceStubVtbl _IMfxInstrumentsStubVtbl =
{
    &IID_IMfxInstruments,
    &IMfxInstruments_ServerInfo,
    3,
    0, /* pure interpreted */
    CStdStubBuffer_METHODS
};


/* Object interface: IMfxNameListSet, ver. 0.0,
   GUID={0x24385AC1,0x02A8,0x11D2,{0xA8,0xE0,0x00,0x00,0xA0,0x09,0x0B,0x97}} */

#pragma code_seg(".orpc")
static const unsigned short IMfxNameListSet_FormatStringOffsetTable[] =
    {
    0
    };

static const MIDL_STUBLESS_PROXY_INFO IMfxNameListSet_ProxyInfo =
    {
    &Object_StubDesc,
    mfx__MIDL_ProcFormatString.Format,
    &IMfxNameListSet_FormatStringOffsetTable[-3],
    0,
    0,
    0
    };


static const MIDL_SERVER_INFO IMfxNameListSet_ServerInfo = 
    {
    &Object_StubDesc,
    0,
    mfx__MIDL_ProcFormatString.Format,
    &IMfxNameListSet_FormatStringOffsetTable[-3],
    0,
    0,
    0,
    0};
CINTERFACE_PROXY_VTABLE(3) _IMfxNameListSetProxyVtbl = 
{
    0,
    &IID_IMfxNameListSet,
    IUnknown_QueryInterface_Proxy,
    IUnknown_AddRef_Proxy,
    IUnknown_Release_Proxy
};

const CInterfaceStubVtbl _IMfxNameListSetStubVtbl =
{
    &IID_IMfxNameListSet,
    &IMfxNameListSet_ServerInfo,
    3,
    0, /* pure interpreted */
    CStdStubBuffer_METHODS
};


/* Object interface: IMfxNameList, ver. 0.0,
   GUID={0x857F3281,0x02A9,0x11D2,{0xA8,0xE0,0x00,0x00,0xA0,0x09,0x0B,0x97}} */

#pragma code_seg(".orpc")
static const unsigned short IMfxNameList_FormatStringOffsetTable[] =
    {
    0
    };

static const MIDL_STUBLESS_PROXY_INFO IMfxNameList_ProxyInfo =
    {
    &Object_StubDesc,
    mfx__MIDL_ProcFormatString.Format,
    &IMfxNameList_FormatStringOffsetTable[-3],
    0,
    0,
    0
    };


static const MIDL_SERVER_INFO IMfxNameList_ServerInfo = 
    {
    &Object_StubDesc,
    0,
    mfx__MIDL_ProcFormatString.Format,
    &IMfxNameList_FormatStringOffsetTable[-3],
    0,
    0,
    0,
    0};
CINTERFACE_PROXY_VTABLE(3) _IMfxNameListProxyVtbl = 
{
    0,
    &IID_IMfxNameList,
    IUnknown_QueryInterface_Proxy,
    IUnknown_AddRef_Proxy,
    IUnknown_Release_Proxy
};

const CInterfaceStubVtbl _IMfxNameListStubVtbl =
{
    &IID_IMfxNameList,
    &IMfxNameList_ServerInfo,
    3,
    0, /* pure interpreted */
    CStdStubBuffer_METHODS
};


/* Object interface: IMfxSelection, ver. 0.0,
   GUID={0x838C6961,0x0C50,0x11D2,{0xA8,0xE0,0x00,0x00,0xA0,0x09,0x0D,0xAF}} */

#pragma code_seg(".orpc")
static const unsigned short IMfxSelection_FormatStringOffsetTable[] =
    {
    0
    };

static const MIDL_STUBLESS_PROXY_INFO IMfxSelection_ProxyInfo =
    {
    &Object_StubDesc,
    mfx__MIDL_ProcFormatString.Format,
    &IMfxSelection_FormatStringOffsetTable[-3],
    0,
    0,
    0
    };


static const MIDL_SERVER_INFO IMfxSelection_ServerInfo = 
    {
    &Object_StubDesc,
    0,
    mfx__MIDL_ProcFormatString.Format,
    &IMfxSelection_FormatStringOffsetTable[-3],
    0,
    0,
    0,
    0};
CINTERFACE_PROXY_VTABLE(3) _IMfxSelectionProxyVtbl = 
{
    0,
    &IID_IMfxSelection,
    IUnknown_QueryInterface_Proxy,
    IUnknown_AddRef_Proxy,
    IUnknown_Release_Proxy
};

const CInterfaceStubVtbl _IMfxSelectionStubVtbl =
{
    &IID_IMfxSelection,
    &IMfxSelection_ServerInfo,
    3,
    0, /* pure interpreted */
    CStdStubBuffer_METHODS
};


/* Object interface: IMfxInputPulse, ver. 0.0,
   GUID={0xF5C46121,0x3C4A,0x11D2,{0xA8,0xE0,0x00,0x00,0xA0,0x09,0x0D,0xAF}} */

#pragma code_seg(".orpc")
static const unsigned short IMfxInputPulse_FormatStringOffsetTable[] =
    {
    0
    };

static const MIDL_STUBLESS_PROXY_INFO IMfxInputPulse_ProxyInfo =
    {
    &Object_StubDesc,
    mfx__MIDL_ProcFormatString.Format,
    &IMfxInputPulse_FormatStringOffsetTable[-3],
    0,
    0,
    0
    };


static const MIDL_SERVER_INFO IMfxInputPulse_ServerInfo = 
    {
    &Object_StubDesc,
    0,
    mfx__MIDL_ProcFormatString.Format,
    &IMfxInputPulse_FormatStringOffsetTable[-3],
    0,
    0,
    0,
    0};
CINTERFACE_PROXY_VTABLE(3) _IMfxInputPulseProxyVtbl = 
{
    0,
    &IID_IMfxInputPulse,
    IUnknown_QueryInterface_Proxy,
    IUnknown_AddRef_Proxy,
    IUnknown_Release_Proxy
};

const CInterfaceStubVtbl _IMfxInputPulseStubVtbl =
{
    &IID_IMfxInputPulse,
    &IMfxInputPulse_ServerInfo,
    3,
    0, /* pure interpreted */
    CStdStubBuffer_METHODS
};


/* Object interface: IMfxDocumentFactory, ver. 0.0,
   GUID={0xF228EE01,0xDE34,0x11D2,{0xA8,0xE0,0x00,0x00,0xA0,0x09,0x0D,0xAF}} */

#pragma code_seg(".orpc")
static const unsigned short IMfxDocumentFactory_FormatStringOffsetTable[] =
    {
    0
    };

static const MIDL_STUBLESS_PROXY_INFO IMfxDocumentFactory_ProxyInfo =
    {
    &Object_StubDesc,
    mfx__MIDL_ProcFormatString.Format,
    &IMfxDocumentFactory_FormatStringOffsetTable[-3],
    0,
    0,
    0
    };


static const MIDL_SERVER_INFO IMfxDocumentFactory_ServerInfo = 
    {
    &Object_StubDesc,
    0,
    mfx__MIDL_ProcFormatString.Format,
    &IMfxDocumentFactory_FormatStringOffsetTable[-3],
    0,
    0,
    0,
    0};
CINTERFACE_PROXY_VTABLE(3) _IMfxDocumentFactoryProxyVtbl = 
{
    0,
    &IID_IMfxDocumentFactory,
    IUnknown_QueryInterface_Proxy,
    IUnknown_AddRef_Proxy,
    IUnknown_Release_Proxy
};

const CInterfaceStubVtbl _IMfxDocumentFactoryStubVtbl =
{
    &IID_IMfxDocumentFactory,
    &IMfxDocumentFactory_ServerInfo,
    3,
    0, /* pure interpreted */
    CStdStubBuffer_METHODS
};


/* Object interface: IMfxDocument, ver. 0.0,
   GUID={0x089058C1,0xEEB3,0x11D2,{0xA8,0xE0,0x00,0x00,0xA0,0x09,0x0D,0xAF}} */

#pragma code_seg(".orpc")
static const unsigned short IMfxDocument_FormatStringOffsetTable[] =
    {
    0
    };

static const MIDL_STUBLESS_PROXY_INFO IMfxDocument_ProxyInfo =
    {
    &Object_StubDesc,
    mfx__MIDL_ProcFormatString.Format,
    &IMfxDocument_FormatStringOffsetTable[-3],
    0,
    0,
    0
    };


static const MIDL_SERVER_INFO IMfxDocument_ServerInfo = 
    {
    &Object_StubDesc,
    0,
    mfx__MIDL_ProcFormatString.Format,
    &IMfxDocument_FormatStringOffsetTable[-3],
    0,
    0,
    0,
    0};
CINTERFACE_PROXY_VTABLE(3) _IMfxDocumentProxyVtbl = 
{
    0,
    &IID_IMfxDocument,
    IUnknown_QueryInterface_Proxy,
    IUnknown_AddRef_Proxy,
    IUnknown_Release_Proxy
};

const CInterfaceStubVtbl _IMfxDocumentStubVtbl =
{
    &IID_IMfxDocument,
    &IMfxDocument_ServerInfo,
    3,
    0, /* pure interpreted */
    CStdStubBuffer_METHODS
};


/* Object interface: IMfxMarkerMap, ver. 0.0,
   GUID={0x1DA99B21,0xE091,0x11D2,{0xA8,0xE0,0x00,0x00,0xA0,0x09,0x0B,0x97}} */

#pragma code_seg(".orpc")
static const unsigned short IMfxMarkerMap_FormatStringOffsetTable[] =
    {
    0
    };

static const MIDL_STUBLESS_PROXY_INFO IMfxMarkerMap_ProxyInfo =
    {
    &Object_StubDesc,
    mfx__MIDL_ProcFormatString.Format,
    &IMfxMarkerMap_FormatStringOffsetTable[-3],
    0,
    0,
    0
    };


static const MIDL_SERVER_INFO IMfxMarkerMap_ServerInfo = 
    {
    &Object_StubDesc,
    0,
    mfx__MIDL_ProcFormatString.Format,
    &IMfxMarkerMap_FormatStringOffsetTable[-3],
    0,
    0,
    0,
    0};
CINTERFACE_PROXY_VTABLE(3) _IMfxMarkerMapProxyVtbl = 
{
    0,
    &IID_IMfxMarkerMap,
    IUnknown_QueryInterface_Proxy,
    IUnknown_AddRef_Proxy,
    IUnknown_Release_Proxy
};

const CInterfaceStubVtbl _IMfxMarkerMapStubVtbl =
{
    &IID_IMfxMarkerMap,
    &IMfxMarkerMap_ServerInfo,
    3,
    0, /* pure interpreted */
    CStdStubBuffer_METHODS
};


/* Object interface: IMfxBufferFactory, ver. 0.0,
   GUID={0xC779B641,0xEDE2,0x11D2,{0xA8,0xE0,0x00,0x00,0xA0,0x09,0x0D,0xAF}} */

#pragma code_seg(".orpc")
static const unsigned short IMfxBufferFactory_FormatStringOffsetTable[] =
    {
    0
    };

static const MIDL_STUBLESS_PROXY_INFO IMfxBufferFactory_ProxyInfo =
    {
    &Object_StubDesc,
    mfx__MIDL_ProcFormatString.Format,
    &IMfxBufferFactory_FormatStringOffsetTable[-3],
    0,
    0,
    0
    };


static const MIDL_SERVER_INFO IMfxBufferFactory_ServerInfo = 
    {
    &Object_StubDesc,
    0,
    mfx__MIDL_ProcFormatString.Format,
    &IMfxBufferFactory_FormatStringOffsetTable[-3],
    0,
    0,
    0,
    0};
CINTERFACE_PROXY_VTABLE(3) _IMfxBufferFactoryProxyVtbl = 
{
    0,
    &IID_IMfxBufferFactory,
    IUnknown_QueryInterface_Proxy,
    IUnknown_AddRef_Proxy,
    IUnknown_Release_Proxy
};

const CInterfaceStubVtbl _IMfxBufferFactoryStubVtbl =
{
    &IID_IMfxBufferFactory,
    &IMfxBufferFactory_ServerInfo,
    3,
    0, /* pure interpreted */
    CStdStubBuffer_METHODS
};


/* Object interface: IMfxEventQueue2, ver. 0.0,
   GUID={0xC51273A1,0xF0EA,0x11d2,{0xA8,0xE0,0x00,0x00,0xA0,0x09,0x0D,0xAF}} */

static const MIDL_STUB_DESC Object_StubDesc = 
    {
    0,
    NdrOleAllocate,
    NdrOleFree,
    0,
    0,
    0,
    0,
    0,
    mfx__MIDL_TypeFormatString.Format,
    1, /* -error bounds_check flag */
    0x50002, /* Ndr library version */
    0,
    0x700022b, /* MIDL Version 7.0.555 */
    0,
    0,
    0,  /* notify & notify_flag routine table */
    0x1, /* MIDL flag */
    0, /* cs routines */
    0,   /* proxy/server info */
    0
    };

const CInterfaceProxyVtbl * const _mfx_ProxyVtblList[] = 
{
    ( CInterfaceProxyVtbl *) &_IMfxInstrumentsProxyVtbl,
    ( CInterfaceProxyVtbl *) &_IMfxKeySigMapProxyVtbl,
    ( CInterfaceProxyVtbl *) &_IMfxDocumentFactoryProxyVtbl,
    ( CInterfaceProxyVtbl *) &_IMfxInputPulseProxyVtbl,
    ( CInterfaceProxyVtbl *) &_IMfxMarkerMapProxyVtbl,
    ( CInterfaceProxyVtbl *) &_IMfxBufferFactoryProxyVtbl,
    ( CInterfaceProxyVtbl *) &_IMfxSelectionProxyVtbl,
    ( CInterfaceProxyVtbl *) &_IMfxNameListProxyVtbl,
    ( CInterfaceProxyVtbl *) &_IMfxDocumentProxyVtbl,
    ( CInterfaceProxyVtbl *) &_IMfxNameListSetProxyVtbl,
    0
};

const CInterfaceStubVtbl * const _mfx_StubVtblList[] = 
{
    ( CInterfaceStubVtbl *) &_IMfxInstrumentsStubVtbl,
    ( CInterfaceStubVtbl *) &_IMfxKeySigMapStubVtbl,
    ( CInterfaceStubVtbl *) &_IMfxDocumentFactoryStubVtbl,
    ( CInterfaceStubVtbl *) &_IMfxInputPulseStubVtbl,
    ( CInterfaceStubVtbl *) &_IMfxMarkerMapStubVtbl,
    ( CInterfaceStubVtbl *) &_IMfxBufferFactoryStubVtbl,
    ( CInterfaceStubVtbl *) &_IMfxSelectionStubVtbl,
    ( CInterfaceStubVtbl *) &_IMfxNameListStubVtbl,
    ( CInterfaceStubVtbl *) &_IMfxDocumentStubVtbl,
    ( CInterfaceStubVtbl *) &_IMfxNameListSetStubVtbl,
    0
};

PCInterfaceName const _mfx_InterfaceNamesList[] = 
{
    "IMfxInstruments",
    "IMfxKeySigMap",
    "IMfxDocumentFactory",
    "IMfxInputPulse",
    "IMfxMarkerMap",
    "IMfxBufferFactory",
    "IMfxSelection",
    "IMfxNameList",
    "IMfxDocument",
    "IMfxNameListSet",
    0
};


#define _mfx_CHECK_IID(n)	IID_GENERIC_CHECK_IID( _mfx, pIID, n)

int __stdcall _mfx_IID_Lookup( const IID * pIID, int * pIndex )
{
    IID_BS_LOOKUP_SETUP

    IID_BS_LOOKUP_INITIAL_TEST( _mfx, 10, 8 )
    IID_BS_LOOKUP_NEXT_TEST( _mfx, 4 )
    IID_BS_LOOKUP_NEXT_TEST( _mfx, 2 )
    IID_BS_LOOKUP_NEXT_TEST( _mfx, 1 )
    IID_BS_LOOKUP_RETURN_RESULT( _mfx, 10, *pIndex )
    
}

const ExtendedProxyFileInfo mfx_ProxyFileInfo = 
{
    (PCInterfaceProxyVtblList *) & _mfx_ProxyVtblList,
    (PCInterfaceStubVtblList *) & _mfx_StubVtblList,
    (const PCInterfaceName * ) & _mfx_InterfaceNamesList,
    0, /* no delegation */
    & _mfx_IID_Lookup, 
    10,
    2,
    0, /* table of [async_uuid] interfaces */
    0, /* Filler1 */
    0, /* Filler2 */
    0  /* Filler3 */
};
#pragma optimize("", on )
#if _MSC_VER >= 1200
#pragma warning(pop)
#endif


#endif /* !defined(_M_IA64) && !defined(_M_AMD64)*/

