

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Thu May 09 21:40:24 2013
 */
/* Compiler settings for mfx.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __mfx_h_h__
#define __mfx_h_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IMfxEventFilter_FWD_DEFINED__
#define __IMfxEventFilter_FWD_DEFINED__
typedef interface IMfxEventFilter IMfxEventFilter;
#endif 	/* __IMfxEventFilter_FWD_DEFINED__ */


#ifndef __IMfxEventQueue_FWD_DEFINED__
#define __IMfxEventQueue_FWD_DEFINED__
typedef interface IMfxEventQueue IMfxEventQueue;
#endif 	/* __IMfxEventQueue_FWD_DEFINED__ */


#ifndef __IMfxDataQueue_FWD_DEFINED__
#define __IMfxDataQueue_FWD_DEFINED__
typedef interface IMfxDataQueue IMfxDataQueue;
#endif 	/* __IMfxDataQueue_FWD_DEFINED__ */


#ifndef __IMfxTempoMap_FWD_DEFINED__
#define __IMfxTempoMap_FWD_DEFINED__
typedef interface IMfxTempoMap IMfxTempoMap;
#endif 	/* __IMfxTempoMap_FWD_DEFINED__ */


#ifndef __IMfxMeterMap_FWD_DEFINED__
#define __IMfxMeterMap_FWD_DEFINED__
typedef interface IMfxMeterMap IMfxMeterMap;
#endif 	/* __IMfxMeterMap_FWD_DEFINED__ */


#ifndef __IMfxKeySigMap_FWD_DEFINED__
#define __IMfxKeySigMap_FWD_DEFINED__
typedef interface IMfxKeySigMap IMfxKeySigMap;
#endif 	/* __IMfxKeySigMap_FWD_DEFINED__ */


#ifndef __IMfxInstruments_FWD_DEFINED__
#define __IMfxInstruments_FWD_DEFINED__
typedef interface IMfxInstruments IMfxInstruments;
#endif 	/* __IMfxInstruments_FWD_DEFINED__ */


#ifndef __IMfxNameListSet_FWD_DEFINED__
#define __IMfxNameListSet_FWD_DEFINED__
typedef interface IMfxNameListSet IMfxNameListSet;
#endif 	/* __IMfxNameListSet_FWD_DEFINED__ */


#ifndef __IMfxNameList_FWD_DEFINED__
#define __IMfxNameList_FWD_DEFINED__
typedef interface IMfxNameList IMfxNameList;
#endif 	/* __IMfxNameList_FWD_DEFINED__ */


#ifndef __IMfxSelection_FWD_DEFINED__
#define __IMfxSelection_FWD_DEFINED__
typedef interface IMfxSelection IMfxSelection;
#endif 	/* __IMfxSelection_FWD_DEFINED__ */


#ifndef __IMfxInputPulse_FWD_DEFINED__
#define __IMfxInputPulse_FWD_DEFINED__
typedef interface IMfxInputPulse IMfxInputPulse;
#endif 	/* __IMfxInputPulse_FWD_DEFINED__ */


#ifndef __IMfxDocumentFactory_FWD_DEFINED__
#define __IMfxDocumentFactory_FWD_DEFINED__
typedef interface IMfxDocumentFactory IMfxDocumentFactory;
#endif 	/* __IMfxDocumentFactory_FWD_DEFINED__ */


#ifndef __IMfxDocument_FWD_DEFINED__
#define __IMfxDocument_FWD_DEFINED__
typedef interface IMfxDocument IMfxDocument;
#endif 	/* __IMfxDocument_FWD_DEFINED__ */


#ifndef __IMfxMarkerMap_FWD_DEFINED__
#define __IMfxMarkerMap_FWD_DEFINED__
typedef interface IMfxMarkerMap IMfxMarkerMap;
#endif 	/* __IMfxMarkerMap_FWD_DEFINED__ */


#ifndef __IMfxBufferFactory_FWD_DEFINED__
#define __IMfxBufferFactory_FWD_DEFINED__
typedef interface IMfxBufferFactory IMfxBufferFactory;
#endif 	/* __IMfxBufferFactory_FWD_DEFINED__ */


#ifndef __IMfxEventQueue2_FWD_DEFINED__
#define __IMfxEventQueue2_FWD_DEFINED__
typedef interface IMfxEventQueue2 IMfxEventQueue2;
#endif 	/* __IMfxEventQueue2_FWD_DEFINED__ */


/* header files for imported files */
#include "unknwn.h"

#ifdef __cplusplus
extern "C"{
#endif 


/* interface __MIDL_itf_mfx_0000_0000 */
/* [local] */ 
















union MfxDataUnion
    {
    unsigned long m_dwData;
    } ;
struct MfxData
    {
    LONG m_lTime;
    union MfxDataUnion m_u1;
    } ;


extern RPC_IF_HANDLE __MIDL_itf_mfx_0000_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_mfx_0000_0000_v0_0_s_ifspec;

#ifndef __IMfxEventFilter_INTERFACE_DEFINED__
#define __IMfxEventFilter_INTERFACE_DEFINED__

/* interface IMfxEventFilter */
/* [uuid][local][object] */ 


EXTERN_C const IID IID_IMfxEventFilter;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("8D7FDE01-1B1B-11D2-A8E0-0000A0090DAF")
    IMfxEventFilter : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE Connect( 
            /* [in] */ IUnknown *pContext) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Disconnect( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE OnStart( 
            /* [in] */ long lTime,
            /* [in] */ IMfxEventQueue *pqOut) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE OnLoop( 
            /* [in] */ long lTimeRestart,
            /* [in] */ long lTimeStop,
            /* [in] */ IMfxEventQueue *pqOut) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE OnStop( 
            /* [in] */ long lTime,
            /* [in] */ IMfxEventQueue *pqOut) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE OnEvents( 
            /* [in] */ long lTimeFrom,
            /* [in] */ long lTimeThru,
            /* [in] */ IMfxEventQueue *pqIn,
            /* [in] */ IMfxEventQueue *pqOut) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE OnInput( 
            /* [in] */ IMfxDataQueue *pqIn,
            /* [in] */ IMfxDataQueue *pqOut) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMfxEventFilterVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMfxEventFilter * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMfxEventFilter * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMfxEventFilter * This);
        
        HRESULT ( STDMETHODCALLTYPE *Connect )( 
            IMfxEventFilter * This,
            /* [in] */ IUnknown *pContext);
        
        HRESULT ( STDMETHODCALLTYPE *Disconnect )( 
            IMfxEventFilter * This);
        
        HRESULT ( STDMETHODCALLTYPE *OnStart )( 
            IMfxEventFilter * This,
            /* [in] */ long lTime,
            /* [in] */ IMfxEventQueue *pqOut);
        
        HRESULT ( STDMETHODCALLTYPE *OnLoop )( 
            IMfxEventFilter * This,
            /* [in] */ long lTimeRestart,
            /* [in] */ long lTimeStop,
            /* [in] */ IMfxEventQueue *pqOut);
        
        HRESULT ( STDMETHODCALLTYPE *OnStop )( 
            IMfxEventFilter * This,
            /* [in] */ long lTime,
            /* [in] */ IMfxEventQueue *pqOut);
        
        HRESULT ( STDMETHODCALLTYPE *OnEvents )( 
            IMfxEventFilter * This,
            /* [in] */ long lTimeFrom,
            /* [in] */ long lTimeThru,
            /* [in] */ IMfxEventQueue *pqIn,
            /* [in] */ IMfxEventQueue *pqOut);
        
        HRESULT ( STDMETHODCALLTYPE *OnInput )( 
            IMfxEventFilter * This,
            /* [in] */ IMfxDataQueue *pqIn,
            /* [in] */ IMfxDataQueue *pqOut);
        
        END_INTERFACE
    } IMfxEventFilterVtbl;

    interface IMfxEventFilter
    {
        CONST_VTBL struct IMfxEventFilterVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMfxEventFilter_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMfxEventFilter_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMfxEventFilter_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IMfxEventFilter_Connect(This,pContext)	\
    ( (This)->lpVtbl -> Connect(This,pContext) ) 

#define IMfxEventFilter_Disconnect(This)	\
    ( (This)->lpVtbl -> Disconnect(This) ) 

#define IMfxEventFilter_OnStart(This,lTime,pqOut)	\
    ( (This)->lpVtbl -> OnStart(This,lTime,pqOut) ) 

#define IMfxEventFilter_OnLoop(This,lTimeRestart,lTimeStop,pqOut)	\
    ( (This)->lpVtbl -> OnLoop(This,lTimeRestart,lTimeStop,pqOut) ) 

#define IMfxEventFilter_OnStop(This,lTime,pqOut)	\
    ( (This)->lpVtbl -> OnStop(This,lTime,pqOut) ) 

#define IMfxEventFilter_OnEvents(This,lTimeFrom,lTimeThru,pqIn,pqOut)	\
    ( (This)->lpVtbl -> OnEvents(This,lTimeFrom,lTimeThru,pqIn,pqOut) ) 

#define IMfxEventFilter_OnInput(This,pqIn,pqOut)	\
    ( (This)->lpVtbl -> OnInput(This,pqIn,pqOut) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMfxEventFilter_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_mfx_0000_0001 */
/* [local] */ 


enum MfxEvent_Type
    {	Note	= 0,
	KeyAft	= ( Note + 1 ) ,
	Control	= ( KeyAft + 1 ) ,
	Patch	= ( Control + 1 ) ,
	ChanAft	= ( Patch + 1 ) ,
	Wheel	= ( ChanAft + 1 ) ,
	RPN	= ( Wheel + 1 ) ,
	NRPN	= ( RPN + 1 ) ,
	Sysx	= ( NRPN + 1 ) ,
	Text	= ( Sysx + 1 ) ,
	Lyric	= ( Text + 1 ) 
    } ;
typedef BYTE MfxMuteMask;

struct MfxEvent
    {
    LONG m_lTime;
    union 
        {
        struct 
            {
            BYTE m_byPort;
            BYTE m_byChan;
            } 	;
        struct 
            {
            MfxMuteMask m_maskSet;
            MfxMuteMask m_maskClear;
            } 	;
        small m_nOfs;
        small m_nTrim;
        } 	;
    enum MfxEvent_Type m_eType;
    /* [switch_type] */ union 
        {
        struct 
            {
            BYTE m_byKey;
            BYTE m_byVel;
            BYTE m_byVelOff;
            DWORD m_dwDuration;
            } 	;
        } 	;
    } ;


extern RPC_IF_HANDLE __MIDL_itf_mfx_0000_0001_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_mfx_0000_0001_v0_0_s_ifspec;

#ifndef __IMfxEventQueue_INTERFACE_DEFINED__
#define __IMfxEventQueue_INTERFACE_DEFINED__

/* interface IMfxEventQueue */
/* [uuid][local][object] */ 


EXTERN_C const IID IID_IMfxEventQueue;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("FF76F7C2-F166-11D1-A8E0-0000A0090DAF")
    IMfxEventQueue : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE Add( 
            /* [ref][in] */ struct MfxEvent *event) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetCount( 
            /* [retval][out] */ int *pnCount) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetAt( 
            /* [in] */ int ix,
            /* [retval][out] */ struct MfxEvent *pEvent) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMfxEventQueueVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMfxEventQueue * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMfxEventQueue * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMfxEventQueue * This);
        
        HRESULT ( STDMETHODCALLTYPE *Add )( 
            IMfxEventQueue * This,
            /* [ref][in] */ struct MfxEvent *event);
        
        HRESULT ( STDMETHODCALLTYPE *GetCount )( 
            IMfxEventQueue * This,
            /* [retval][out] */ int *pnCount);
        
        HRESULT ( STDMETHODCALLTYPE *GetAt )( 
            IMfxEventQueue * This,
            /* [in] */ int ix,
            /* [retval][out] */ struct MfxEvent *pEvent);
        
        END_INTERFACE
    } IMfxEventQueueVtbl;

    interface IMfxEventQueue
    {
        CONST_VTBL struct IMfxEventQueueVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMfxEventQueue_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMfxEventQueue_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMfxEventQueue_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IMfxEventQueue_Add(This,event)	\
    ( (This)->lpVtbl -> Add(This,event) ) 

#define IMfxEventQueue_GetCount(This,pnCount)	\
    ( (This)->lpVtbl -> GetCount(This,pnCount) ) 

#define IMfxEventQueue_GetAt(This,ix,pEvent)	\
    ( (This)->lpVtbl -> GetAt(This,ix,pEvent) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMfxEventQueue_INTERFACE_DEFINED__ */


#ifndef __IMfxDataQueue_INTERFACE_DEFINED__
#define __IMfxDataQueue_INTERFACE_DEFINED__

/* interface IMfxDataQueue */
/* [local][object][uuid] */ 


EXTERN_C const IID IID_IMfxDataQueue;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("7A37A621-1B1B-11D2-A8E0-0000A0090DAF")
    IMfxDataQueue : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE Add( 
            /* [ref][in] */ struct MfxData *data) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetCount( 
            /* [retval][out] */ int *pnCount) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetAt( 
            /* [in] */ int ix,
            /* [retval][out] */ struct MfxData *pData) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMfxDataQueueVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMfxDataQueue * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMfxDataQueue * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMfxDataQueue * This);
        
        HRESULT ( STDMETHODCALLTYPE *Add )( 
            IMfxDataQueue * This,
            /* [ref][in] */ struct MfxData *data);
        
        HRESULT ( STDMETHODCALLTYPE *GetCount )( 
            IMfxDataQueue * This,
            /* [retval][out] */ int *pnCount);
        
        HRESULT ( STDMETHODCALLTYPE *GetAt )( 
            IMfxDataQueue * This,
            /* [in] */ int ix,
            /* [retval][out] */ struct MfxData *pData);
        
        END_INTERFACE
    } IMfxDataQueueVtbl;

    interface IMfxDataQueue
    {
        CONST_VTBL struct IMfxDataQueueVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMfxDataQueue_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMfxDataQueue_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMfxDataQueue_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IMfxDataQueue_Add(This,data)	\
    ( (This)->lpVtbl -> Add(This,data) ) 

#define IMfxDataQueue_GetCount(This,pnCount)	\
    ( (This)->lpVtbl -> GetCount(This,pnCount) ) 

#define IMfxDataQueue_GetAt(This,ix,pData)	\
    ( (This)->lpVtbl -> GetAt(This,ix,pData) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMfxDataQueue_INTERFACE_DEFINED__ */


#ifndef __IMfxTempoMap_INTERFACE_DEFINED__
#define __IMfxTempoMap_INTERFACE_DEFINED__

/* interface IMfxTempoMap */
/* [local][object][uuid] */ 


EXTERN_C const IID IID_IMfxTempoMap;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("FF76F7C3-F166-11D1-A8E0-0000A0090DAF")
    IMfxTempoMap : public IUnknown
    {
    public:
        virtual LONG STDMETHODCALLTYPE TicksToMsecs( 
            /* [in] */ LONG lTicks) = 0;
        
        virtual LONG STDMETHODCALLTYPE MsecsToTicks( 
            /* [in] */ LONG lMsecs) = 0;
        
        virtual int STDMETHODCALLTYPE GetTicksPerQuarterNote( void) = 0;
        
        virtual int STDMETHODCALLTYPE GetTempoIndexForTime( 
            /* [in] */ LONG lTicks) = 0;
        
        virtual int STDMETHODCALLTYPE GetTempoCount( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetTempoAt( 
            /* [in] */ int ix,
            /* [out] */ LONG *plTicks,
            /* [out] */ int *pnBPM100) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMfxTempoMapVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMfxTempoMap * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMfxTempoMap * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMfxTempoMap * This);
        
        LONG ( STDMETHODCALLTYPE *TicksToMsecs )( 
            IMfxTempoMap * This,
            /* [in] */ LONG lTicks);
        
        LONG ( STDMETHODCALLTYPE *MsecsToTicks )( 
            IMfxTempoMap * This,
            /* [in] */ LONG lMsecs);
        
        int ( STDMETHODCALLTYPE *GetTicksPerQuarterNote )( 
            IMfxTempoMap * This);
        
        int ( STDMETHODCALLTYPE *GetTempoIndexForTime )( 
            IMfxTempoMap * This,
            /* [in] */ LONG lTicks);
        
        int ( STDMETHODCALLTYPE *GetTempoCount )( 
            IMfxTempoMap * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTempoAt )( 
            IMfxTempoMap * This,
            /* [in] */ int ix,
            /* [out] */ LONG *plTicks,
            /* [out] */ int *pnBPM100);
        
        END_INTERFACE
    } IMfxTempoMapVtbl;

    interface IMfxTempoMap
    {
        CONST_VTBL struct IMfxTempoMapVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMfxTempoMap_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMfxTempoMap_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMfxTempoMap_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IMfxTempoMap_TicksToMsecs(This,lTicks)	\
    ( (This)->lpVtbl -> TicksToMsecs(This,lTicks) ) 

#define IMfxTempoMap_MsecsToTicks(This,lMsecs)	\
    ( (This)->lpVtbl -> MsecsToTicks(This,lMsecs) ) 

#define IMfxTempoMap_GetTicksPerQuarterNote(This)	\
    ( (This)->lpVtbl -> GetTicksPerQuarterNote(This) ) 

#define IMfxTempoMap_GetTempoIndexForTime(This,lTicks)	\
    ( (This)->lpVtbl -> GetTempoIndexForTime(This,lTicks) ) 

#define IMfxTempoMap_GetTempoCount(This)	\
    ( (This)->lpVtbl -> GetTempoCount(This) ) 

#define IMfxTempoMap_GetTempoAt(This,ix,plTicks,pnBPM100)	\
    ( (This)->lpVtbl -> GetTempoAt(This,ix,plTicks,pnBPM100) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMfxTempoMap_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_mfx_0000_0004 */
/* [local] */ 


enum EBeatValue
    {	Beat1	= 0,
	Beat2	= 1,
	Beat4	= 2,
	Beat8	= 3,
	Beat16	= 4,
	Beat32	= 5
    } ;


extern RPC_IF_HANDLE __MIDL_itf_mfx_0000_0004_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_mfx_0000_0004_v0_0_s_ifspec;

#ifndef __IMfxMeterMap_INTERFACE_DEFINED__
#define __IMfxMeterMap_INTERFACE_DEFINED__

/* interface IMfxMeterMap */
/* [local][object][uuid] */ 


EXTERN_C const IID IID_IMfxMeterMap;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("FF76F7C4-F166-11D1-A8E0-0000A0090DAF")
    IMfxMeterMap : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE TicksToMBT( 
            /* [in] */ LONG lTicks,
            /* [out] */ int *pnMeasure,
            /* [out] */ int *pnBeat,
            /* [out] */ int *pnTicks) = 0;
        
        virtual LONG STDMETHODCALLTYPE MBTToTicks( 
            /* [in] */ int nMeasure,
            /* [in] */ int nBeat,
            /* [in] */ int nTicks) = 0;
        
        virtual int STDMETHODCALLTYPE GetMeterIndexForTime( 
            /* [in] */ LONG lTicks) = 0;
        
        virtual int STDMETHODCALLTYPE GetMeterCount( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetMeterAt( 
            /* [in] */ int ix,
            /* [out] */ int *pnMeasure,
            /* [out] */ int *pnTop,
            /* [out] */ enum EBeatValue *eBottom) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMfxMeterMapVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMfxMeterMap * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMfxMeterMap * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMfxMeterMap * This);
        
        HRESULT ( STDMETHODCALLTYPE *TicksToMBT )( 
            IMfxMeterMap * This,
            /* [in] */ LONG lTicks,
            /* [out] */ int *pnMeasure,
            /* [out] */ int *pnBeat,
            /* [out] */ int *pnTicks);
        
        LONG ( STDMETHODCALLTYPE *MBTToTicks )( 
            IMfxMeterMap * This,
            /* [in] */ int nMeasure,
            /* [in] */ int nBeat,
            /* [in] */ int nTicks);
        
        int ( STDMETHODCALLTYPE *GetMeterIndexForTime )( 
            IMfxMeterMap * This,
            /* [in] */ LONG lTicks);
        
        int ( STDMETHODCALLTYPE *GetMeterCount )( 
            IMfxMeterMap * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetMeterAt )( 
            IMfxMeterMap * This,
            /* [in] */ int ix,
            /* [out] */ int *pnMeasure,
            /* [out] */ int *pnTop,
            /* [out] */ enum EBeatValue *eBottom);
        
        END_INTERFACE
    } IMfxMeterMapVtbl;

    interface IMfxMeterMap
    {
        CONST_VTBL struct IMfxMeterMapVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMfxMeterMap_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMfxMeterMap_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMfxMeterMap_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IMfxMeterMap_TicksToMBT(This,lTicks,pnMeasure,pnBeat,pnTicks)	\
    ( (This)->lpVtbl -> TicksToMBT(This,lTicks,pnMeasure,pnBeat,pnTicks) ) 

#define IMfxMeterMap_MBTToTicks(This,nMeasure,nBeat,nTicks)	\
    ( (This)->lpVtbl -> MBTToTicks(This,nMeasure,nBeat,nTicks) ) 

#define IMfxMeterMap_GetMeterIndexForTime(This,lTicks)	\
    ( (This)->lpVtbl -> GetMeterIndexForTime(This,lTicks) ) 

#define IMfxMeterMap_GetMeterCount(This)	\
    ( (This)->lpVtbl -> GetMeterCount(This) ) 

#define IMfxMeterMap_GetMeterAt(This,ix,pnMeasure,pnTop,eBottom)	\
    ( (This)->lpVtbl -> GetMeterAt(This,ix,pnMeasure,pnTop,eBottom) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMfxMeterMap_INTERFACE_DEFINED__ */


#ifndef __IMfxKeySigMap_INTERFACE_DEFINED__
#define __IMfxKeySigMap_INTERFACE_DEFINED__

/* interface IMfxKeySigMap */
/* [object][uuid] */ 


EXTERN_C const IID IID_IMfxKeySigMap;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("A489E601-0247-11D2-A8E0-0000A0090B97")
    IMfxKeySigMap : public IUnknown
    {
    public:
    };
    
#else 	/* C style interface */

    typedef struct IMfxKeySigMapVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMfxKeySigMap * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMfxKeySigMap * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMfxKeySigMap * This);
        
        END_INTERFACE
    } IMfxKeySigMapVtbl;

    interface IMfxKeySigMap
    {
        CONST_VTBL struct IMfxKeySigMapVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMfxKeySigMap_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMfxKeySigMap_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMfxKeySigMap_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMfxKeySigMap_INTERFACE_DEFINED__ */


#ifndef __IMfxInstruments_INTERFACE_DEFINED__
#define __IMfxInstruments_INTERFACE_DEFINED__

/* interface IMfxInstruments */
/* [object][uuid] */ 


EXTERN_C const IID IID_IMfxInstruments;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("6BFCA001-043D-11D2-A8E0-0000A0090DAF")
    IMfxInstruments : public IUnknown
    {
    public:
    };
    
#else 	/* C style interface */

    typedef struct IMfxInstrumentsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMfxInstruments * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMfxInstruments * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMfxInstruments * This);
        
        END_INTERFACE
    } IMfxInstrumentsVtbl;

    interface IMfxInstruments
    {
        CONST_VTBL struct IMfxInstrumentsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMfxInstruments_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMfxInstruments_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMfxInstruments_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMfxInstruments_INTERFACE_DEFINED__ */


#ifndef __IMfxNameListSet_INTERFACE_DEFINED__
#define __IMfxNameListSet_INTERFACE_DEFINED__

/* interface IMfxNameListSet */
/* [object][uuid] */ 


EXTERN_C const IID IID_IMfxNameListSet;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("24385AC1-02A8-11D2-A8E0-0000A0090B97")
    IMfxNameListSet : public IUnknown
    {
    public:
    };
    
#else 	/* C style interface */

    typedef struct IMfxNameListSetVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMfxNameListSet * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMfxNameListSet * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMfxNameListSet * This);
        
        END_INTERFACE
    } IMfxNameListSetVtbl;

    interface IMfxNameListSet
    {
        CONST_VTBL struct IMfxNameListSetVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMfxNameListSet_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMfxNameListSet_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMfxNameListSet_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMfxNameListSet_INTERFACE_DEFINED__ */


#ifndef __IMfxNameList_INTERFACE_DEFINED__
#define __IMfxNameList_INTERFACE_DEFINED__

/* interface IMfxNameList */
/* [object][uuid] */ 


EXTERN_C const IID IID_IMfxNameList;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("857F3281-02A9-11D2-A8E0-0000A0090B97")
    IMfxNameList : public IUnknown
    {
    public:
    };
    
#else 	/* C style interface */

    typedef struct IMfxNameListVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMfxNameList * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMfxNameList * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMfxNameList * This);
        
        END_INTERFACE
    } IMfxNameListVtbl;

    interface IMfxNameList
    {
        CONST_VTBL struct IMfxNameListVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMfxNameList_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMfxNameList_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMfxNameList_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMfxNameList_INTERFACE_DEFINED__ */


#ifndef __IMfxSelection_INTERFACE_DEFINED__
#define __IMfxSelection_INTERFACE_DEFINED__

/* interface IMfxSelection */
/* [object][uuid] */ 


EXTERN_C const IID IID_IMfxSelection;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("838C6961-0C50-11D2-A8E0-0000A0090DAF")
    IMfxSelection : public IUnknown
    {
    public:
    };
    
#else 	/* C style interface */

    typedef struct IMfxSelectionVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMfxSelection * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMfxSelection * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMfxSelection * This);
        
        END_INTERFACE
    } IMfxSelectionVtbl;

    interface IMfxSelection
    {
        CONST_VTBL struct IMfxSelectionVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMfxSelection_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMfxSelection_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMfxSelection_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMfxSelection_INTERFACE_DEFINED__ */


#ifndef __IMfxInputPulse_INTERFACE_DEFINED__
#define __IMfxInputPulse_INTERFACE_DEFINED__

/* interface IMfxInputPulse */
/* [object][uuid] */ 


EXTERN_C const IID IID_IMfxInputPulse;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("F5C46121-3C4A-11D2-A8E0-0000A0090DAF")
    IMfxInputPulse : public IUnknown
    {
    public:
    };
    
#else 	/* C style interface */

    typedef struct IMfxInputPulseVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMfxInputPulse * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMfxInputPulse * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMfxInputPulse * This);
        
        END_INTERFACE
    } IMfxInputPulseVtbl;

    interface IMfxInputPulse
    {
        CONST_VTBL struct IMfxInputPulseVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMfxInputPulse_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMfxInputPulse_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMfxInputPulse_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMfxInputPulse_INTERFACE_DEFINED__ */


#ifndef __IMfxDocumentFactory_INTERFACE_DEFINED__
#define __IMfxDocumentFactory_INTERFACE_DEFINED__

/* interface IMfxDocumentFactory */
/* [object][uuid] */ 


EXTERN_C const IID IID_IMfxDocumentFactory;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("F228EE01-DE34-11D2-A8E0-0000A0090DAF")
    IMfxDocumentFactory : public IUnknown
    {
    public:
    };
    
#else 	/* C style interface */

    typedef struct IMfxDocumentFactoryVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMfxDocumentFactory * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMfxDocumentFactory * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMfxDocumentFactory * This);
        
        END_INTERFACE
    } IMfxDocumentFactoryVtbl;

    interface IMfxDocumentFactory
    {
        CONST_VTBL struct IMfxDocumentFactoryVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMfxDocumentFactory_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMfxDocumentFactory_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMfxDocumentFactory_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMfxDocumentFactory_INTERFACE_DEFINED__ */


#ifndef __IMfxDocument_INTERFACE_DEFINED__
#define __IMfxDocument_INTERFACE_DEFINED__

/* interface IMfxDocument */
/* [object][uuid] */ 


EXTERN_C const IID IID_IMfxDocument;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("089058C1-EEB3-11D2-A8E0-0000A0090DAF")
    IMfxDocument : public IUnknown
    {
    public:
    };
    
#else 	/* C style interface */

    typedef struct IMfxDocumentVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMfxDocument * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMfxDocument * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMfxDocument * This);
        
        END_INTERFACE
    } IMfxDocumentVtbl;

    interface IMfxDocument
    {
        CONST_VTBL struct IMfxDocumentVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMfxDocument_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMfxDocument_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMfxDocument_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMfxDocument_INTERFACE_DEFINED__ */


#ifndef __IMfxMarkerMap_INTERFACE_DEFINED__
#define __IMfxMarkerMap_INTERFACE_DEFINED__

/* interface IMfxMarkerMap */
/* [object][uuid] */ 


EXTERN_C const IID IID_IMfxMarkerMap;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("1DA99B21-E091-11D2-A8E0-0000A0090B97")
    IMfxMarkerMap : public IUnknown
    {
    public:
    };
    
#else 	/* C style interface */

    typedef struct IMfxMarkerMapVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMfxMarkerMap * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMfxMarkerMap * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMfxMarkerMap * This);
        
        END_INTERFACE
    } IMfxMarkerMapVtbl;

    interface IMfxMarkerMap
    {
        CONST_VTBL struct IMfxMarkerMapVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMfxMarkerMap_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMfxMarkerMap_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMfxMarkerMap_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMfxMarkerMap_INTERFACE_DEFINED__ */


#ifndef __IMfxBufferFactory_INTERFACE_DEFINED__
#define __IMfxBufferFactory_INTERFACE_DEFINED__

/* interface IMfxBufferFactory */
/* [object][uuid] */ 


EXTERN_C const IID IID_IMfxBufferFactory;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("C779B641-EDE2-11D2-A8E0-0000A0090DAF")
    IMfxBufferFactory : public IUnknown
    {
    public:
    };
    
#else 	/* C style interface */

    typedef struct IMfxBufferFactoryVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMfxBufferFactory * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMfxBufferFactory * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMfxBufferFactory * This);
        
        END_INTERFACE
    } IMfxBufferFactoryVtbl;

    interface IMfxBufferFactory
    {
        CONST_VTBL struct IMfxBufferFactoryVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMfxBufferFactory_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMfxBufferFactory_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMfxBufferFactory_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMfxBufferFactory_INTERFACE_DEFINED__ */


#ifndef __IMfxEventQueue2_INTERFACE_DEFINED__
#define __IMfxEventQueue2_INTERFACE_DEFINED__

/* interface IMfxEventQueue2 */
/* [local][object][uuid] */ 


EXTERN_C const IID IID_IMfxEventQueue2;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("C51273A1-F0EA-11d2-A8E0-0000A0090DAF")
    IMfxEventQueue2 : public IMfxEventQueue
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE GetBufferFactory( 
            /* [retval][out] */ IMfxBufferFactory **ppIMfxBufferFactory) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMfxEventQueue2Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMfxEventQueue2 * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMfxEventQueue2 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMfxEventQueue2 * This);
        
        HRESULT ( STDMETHODCALLTYPE *Add )( 
            IMfxEventQueue2 * This,
            /* [ref][in] */ struct MfxEvent *event);
        
        HRESULT ( STDMETHODCALLTYPE *GetCount )( 
            IMfxEventQueue2 * This,
            /* [retval][out] */ int *pnCount);
        
        HRESULT ( STDMETHODCALLTYPE *GetAt )( 
            IMfxEventQueue2 * This,
            /* [in] */ int ix,
            /* [retval][out] */ struct MfxEvent *pEvent);
        
        HRESULT ( STDMETHODCALLTYPE *GetBufferFactory )( 
            IMfxEventQueue2 * This,
            /* [retval][out] */ IMfxBufferFactory **ppIMfxBufferFactory);
        
        END_INTERFACE
    } IMfxEventQueue2Vtbl;

    interface IMfxEventQueue2
    {
        CONST_VTBL struct IMfxEventQueue2Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMfxEventQueue2_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMfxEventQueue2_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMfxEventQueue2_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IMfxEventQueue2_Add(This,event)	\
    ( (This)->lpVtbl -> Add(This,event) ) 

#define IMfxEventQueue2_GetCount(This,pnCount)	\
    ( (This)->lpVtbl -> GetCount(This,pnCount) ) 

#define IMfxEventQueue2_GetAt(This,ix,pEvent)	\
    ( (This)->lpVtbl -> GetAt(This,ix,pEvent) ) 


#define IMfxEventQueue2_GetBufferFactory(This,ppIMfxBufferFactory)	\
    ( (This)->lpVtbl -> GetBufferFactory(This,ppIMfxBufferFactory) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMfxEventQueue2_INTERFACE_DEFINED__ */


/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


